GOFILES = $(shell find . -name '*.go' -not -path './vendor/*')
GOPACKAGES = ./domain

default: build

workdir:
	mkdir -p workdir

build: workdir/scraper

workdir/scraper: $(GOFILES)
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o workdir/scraper .

test: test-all

test-all:
	@go test -v $(GOPACKAGES)

lint: lint-all

lint-all:
	@golint -set_exit_status $(GOPACKAGES)