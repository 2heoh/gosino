package domain

import (
	"errors"
)

type Player struct {
	Chips int
	Game  *RollDiceGame
}

func (p Player) ActiveGame() *RollDiceGame {
	return p.Game
}

func (p Player) GetAvailableChips() int {
	return p.Chips
}

func (p Player) IsInGame() bool {
	return p.Game != nil
}

func (p Player) Buy(Chips int) error {
	if Chips < 0 {
		return errors.New("can not bet more than chips available")
	}
	p.Chips = Chips
	return nil
}

func (p Player) Joins(game *RollDiceGame) error {
	if p.IsInGame() {
		return errors.New("player must leave the current game before joining another game")
	}
	p.Game = game
	return nil
}

func (p Player) Bet(bet Bet) error {
	if bet.Amount > p.Chips {
		return errors.New("can not bet more than chips available")
	}

	p.Chips -= bet.Amount
	p.Game.AddBet(p, bet)
	return nil
}

func (p Player) Win(chips int) {
	p.Chips += chips
}

func (p Player) Lose() {

}
