package domain

import "math/rand"

type RollDiceGame struct {
	playerBets map[Player]Bet
}

type Bet struct {
	Amount int
	Score  int
}

func (g *RollDiceGame) AddBet(p Player, bet Bet) {
	g.playerBets[p] = bet
}

func (g *RollDiceGame) Play() {

	winningScore := rand.Intn(6) + 1
	for player := range g.playerBets {

		bet := g.playerBets[player]

		if bet.Score == winningScore {
			player.Win(bet.Amount * 6)
		} else {
			player.Lose()
		}
	}

	g.playerBets = make(map[Player]Bet)

}

func (g *RollDiceGame) Leave(p Player) {
	if _, ok := g.playerBets[p]; ok {
		return
	}

	delete(g.playerBets, p)
}
